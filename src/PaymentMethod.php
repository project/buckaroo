<?php
namespace Drupal\buckaroo;

use Drupal\buckaroo\Config\Config;

interface IPaymentMethod {
  public function handlePayment(string $invoice_id, float $amount, string $currency, array $payment_provider_options): \Buckaroo\Transaction\Response\TransactionResponse;
  public function handleRefund(string $invoice_id, string $transaction_key, float $amount, string $currency): \Buckaroo\Transaction\Response\TransactionResponse;
}

abstract class PaymentMethod implements IPaymentMethod {
  protected string $payment_method;

  protected \Buckaroo\PaymentMethods\PaymentFacade $buckaroo_client;

  public function __construct() {
    $this->buckaroo_client =  $this->getBuckarooClient();
  }

  protected function getBuckarooClient() {
    $config = new Config("buckaroo_integration.settings");

    $client = new \Buckaroo\BuckarooClient(
      $config->get("website_key")->getValue(),
      $config->get("secret_key")->getValue(),
      $config->get("mode")->getValue()
    );

    if($client->confirmCredential() == false)
      throw new \Exception(t("Credentials are invalid"));

    return $client->method($this->payment_method);
  }
}
