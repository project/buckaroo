<?php
namespace Drupal\buckaroo\Config;

use Drupal\Core\Site\Settings;

class Config {
  protected string $config_name;
  protected \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig $config;
  protected array $settings;

  public function __construct(string $config_name) {
    $this->config = \Drupal::getContainer()->get('config.factory')->getEditable($config_name);
    $this->settings = Settings::get($config_name) ?? [];
    $this->config_name = $config_name;
  }

  public function get(string $key) {
    if(isset($this->settings[$key]))
      return new Get(true, $this->settings[$key]);

    return new Get(false, $this->config->get($key));
  }

  public function set(string $key, mixed $value) {
    $current = $this->get($key);
    if($current->getIsReadonly())
      return $this;

    $this->config->set($key, $value);
    return $this;
  }

  public function save() {
    $this->config->save();
  }
}
