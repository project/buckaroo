<?php
namespace Drupal\buckaroo\Config;

class Get {
  protected bool $is_readonly;
  protected mixed $value;

  public function __construct(bool $is_readonly, mixed $value) {
    $this->is_readonly = $is_readonly;
    $this->value = $value;
  }

  public function getIsReadonly() {
    return $this->is_readonly;
  }

  public function getValue() {
    return $this->value;
  }
}
