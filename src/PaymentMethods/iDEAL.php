<?php
namespace Drupal\buckaroo\PaymentMethods;

use Drupal\buckaroo\PaymentMethod;

class iDEAL extends PaymentMethod {
  protected string $payment_method = "ideal";

  public function handlePayment(string $invoice_id, float $amount, string $currency, array $payment_provider_options): \Buckaroo\Transaction\Response\TransactionResponse {
    return $this->buckaroo_client->pay(
      array_merge($payment_provider_options, ["invoice" => $invoice_id, "currency" => $currency, "amountDebit" => $amount]
    ));
  }

  public function handleRefund(string $invoice_id, string $transaction_key, float $amount, string $currency): \Buckaroo\Transaction\Response\TransactionResponse {
    return $this->buckaroo_client->refund([
      "invoice" => $invoice_id,
      "originalTransactionKey" => $transaction_key,
      "amountDebit" => $amount,
      "currency" => $currency
    ]);
  }
}
