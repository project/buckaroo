<?php

namespace Drupal\buckaroo\Controller;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;

class PaymentListBuilder extends EntityListBuilder {
  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [];

    $header['invoice_id'] = [
      'data' => $this->t('Invoice ID'),
      'class' => [RESPONSIVE_PRIORITY_MEDIUM],
    ];
    $header['transaction_key'] = [
      'data' => $this->t('Transaction key'),
      'class' => [RESPONSIVE_PRIORITY_MEDIUM],
    ];
    $header['amount'] = $this->t('Amount');
    $header['amount_refunded'] = [
      'data' => $this->t('Refunded amount'),
      'class' => [RESPONSIVE_PRIORITY_LOW],
    ];
    $header['payment_method'] = [
      'data' => $this->t('Payment method'),
      'class' => [RESPONSIVE_PRIORITY_LOW],
    ];
    $header['status'] = $this->t('Status');

    $header['created'] = [
      'data' => $this->t('Created'),
      'class' => [RESPONSIVE_PRIORITY_LOW],
    ];

    $header['changed'] = [
      'data' => $this->t('Changed'),
      'class' => [RESPONSIVE_PRIORITY_LOW],
    ];

    return $header;
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\buckaroo\Entity\Payment $entity */
    $row = [];

    $row['invoice_id'] = $entity->getInvoiceId();
    $row['transaction_key'] = $entity->getTransactionKey();
    $row['amount'] = $this->t(
      '@amount @currency',
      [
        '@amount' => $entity->getAmount(),
        '@currency' => $entity->getCurrency(),
      ]
    );
    $row['amount_refunded'] = $this->t(
      '@amount @currency',
      [
        '@amount' => $entity->getAmountRefunded() ?? "-",
        '@currency' => $entity->getAmountRefunded() != null ? $entity->getCurrency() : "",
      ]
    );
    $row['payment_method'] = $entity->getPaymentMethod();
    $row['status'] = $entity->getStatus();
    $row['created'] = $this->getFormattedDate($entity->getCreated());
    $row['changed'] = $entity->getChanged() != null ? $this->getFormattedDate($entity->getChanged()) : "";

    return $row;
  }

  /**
   * @param string $date
   *   Date in ISO 8601 format.
   *
   * @return string
   *   Date formatted in medium date format.
   */
  protected function getFormattedDate($date) {
    return (new \DateTime($date))
      ->format('Y-m-d H:i:s');
  }

}
