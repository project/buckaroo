<?php
namespace Drupal\buckaroo\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\buckaroo\Config\Config;

class ConfigForm extends ConfigFormBase {
   /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return "buckaroo_integration_settings";
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ["buckaroo_integration.settings"];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = new Config("buckaroo_integration.settings");

    $form["website_key"] = [
      "#type" => "textfield",
      "#title" => $this->t("Website key"),
      "#default_value" => $config->get("website_key")->getValue() ?? "",
      "#disabled" => $config->get("website_key")->getIsReadonly(),
      "#description" =>
        $config->get("mode") == "live" ?
          $this->t("It is recommended to set this setting via the settings.local.php file!") :
          "",
    ];

    $form["secret_key"] = [
      "#type" => "password",
      "#title" => $this->t("Secret key"),
      "#default_value" => $config->get("secret_key")->getValue() ?? "",
      "#disabled" => $config->get("mode") == "live" || $config->get("secret_key")->getIsReadonly(),
      "#description" =>
        $config->get("mode") == "live" ?
          $this->t("It is required to set this setting via the settings.local.php file. In live mode you can't set this setting via the ui!") :
          $this->t("It is recommended to set this setting via the settings.local.php file!"),
    ];

    $form["mode"] = [
      "#type" => "select",
      "#title" => $this->t("Modes"),
      "#default_value" => $config->get("mode")->getValue() ?? "",
      "#disabled" => $config->get("mode")->getIsReadonly(),
      "#options" => ["test" => $this->t("Test"), "live" => $this->t("Live")],
      '#attributes' => array('onchange' => 'this.form.submit();'),
      "#required" => TRUE,
    ];

    // Code snippet shown on configuration form.
    $codeSnippet = '<code class="prettyprint lang-php">$settings[\'buckaroo_integration.settings\'] = [<br>
      &nbsp;&nbsp;\'website_key\' => \'s_YourSiteKey\',<br>
      &nbsp;&nbsp;\'secret_key\' => \'YourSecretKey\',<br>
      &nbsp;&nbsp;\'mode\' => \'' . ($config->get("mode")->getValue() ?? "test") . '\',<br>
    ];</code>';

    $form['recommend'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Recommended way to set settings'),
      'instruction' => [
        '#markup' => '<p>' . $this->t(
          'For security reasons we recommend that you do not set your settings here. Add your Buckaroo api credentials to the settings.local.php or settings.php file:'
        ) . "</p>$codeSnippet",
      ],
    ];
    $form["#attached"]["library"][] = "buckaroo/config-form";

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   * Check if user"s credentials are valid
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $config = new Config("buckaroo_integration.settings");
    if($form_state->getValue("mode") != $config->get("mode")->getValue() && !$config->get("mode")->getIsReadonly())
      return;

    if($form_state->getValue("website_key") == "" || $form_state->getValue("website_key") == null)
      $form_state->setErrorByName("website_key", $this->t("A website key is required"));

    if($form_state->getValue("secret_key") == "" || $form_state->getValue("secret_key") == null)
      $form_state->setErrorByName("secret_key", $this->t("A secret key is required"));

    $client = new \Buckaroo\BuckarooClient(
      $form_state->getValue("website_key"),
      $form_state->getValue("secret_key"),
      $form_state->getValue("mode")
    );

    if($client->confirmCredential() == false) {
      $form_state->setErrorByName("website_key", $this->t("Credentials are invalid"));
      $form_state->setErrorByName("secret_key", $this->t("Credentials are invalid"));
      $form_state->setErrorByName("mode", $this->t("Credentials are invalid"));
    }

    return parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = new Config("buckaroo_integration.settings");
    if($form_state->getValue("mode") != $config->get("mode")->getValue() && !$config->get("mode")->getIsReadonly())
      $config
        ->set("website_key", "")
        ->set("secret_key", "")
        ->set("mode", $form_state->getValue("mode"))
        ->save();
    else
      $config
        ->set("website_key", $form_state->getValue("website_key"))
        ->set("secret_key", $form_state->getValue("secret_key"))
        ->set("mode", $form_state->getValue("mode"))
        ->save();

    parent::submitForm($form, $form_state);
  }
}
