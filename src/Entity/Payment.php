<?php

namespace Drupal\buckaroo\Entity;

use Exception;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\buckaroo\Config\Config;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;

/**
 * Defines the Payment entity.
 *
 * @ingroup payment
 *
 * @ContentEntityType(
 *   id = "buckaroo_payment",
 *   label = @Translation("Payment"),
 *   base_table = "buckaroo_payment",
 *   entity_keys = {
 *     "id" = "id",
 *     "status" = "status"
 *   },
 *   handlers = {
 *     "list_builder" = "Drupal\buckaroo\Controller\PaymentListBuilder",
 *   },
 * )
 */
class Payment extends ContentEntityBase implements ContentEntityInterface {
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields["id"] = BaseFieldDefinition::create("integer")
      ->setLabel("ID")
      ->setRequired(TRUE)
      ->setDescription(t("The ID of the Payment entity."))
      ->setReadOnly(true);

    $fields["invoice_id"] = BaseFieldDefinition::create("string")
      ->setLabel("Invoice ID")
      ->setRequired(TRUE)
      ->setDescription(t("The invoice ID of the payment."));

    $fields["transaction_key"] = BaseFieldDefinition::create("string")
      ->setLabel("Transaction key")
      ->setDescription(t("The transaction key of the payment."));

    $fields["amount"] = BaseFieldDefinition::create("float")
      ->setLabel("Amount")
      ->setRequired(TRUE)
      ->setDescription(t("The amount that has been payed."));

    $fields["amount_refunded"] = BaseFieldDefinition::create("float")
      ->setLabel("Refunded amount")
      ->setDescription(t("The amount that has been returned to the user."));

    $fields["currency"] = BaseFieldDefinition::create("string")
      ->setLabel("Currency")
      ->setRequired(TRUE)
      ->setDescription(t("The currency the user has to use to pay."));

    $fields["payment_method"] = BaseFieldDefinition::create("string")
      ->setLabel("Payment method")
      ->setRequired(TRUE)
      ->setDescription(t("The payment method that has been used to pay."));

    $fields["status"] = BaseFieldDefinition::create("integer")
      ->setLabel("Status")
      ->setRequired(TRUE)
      ->setDescription(t("The status of the payment"))
      ->setDefaultValue(-1);

    $config = new Config("buckaroo_integration.settings");
    $fields["mode"] = BaseFieldDefinition::create("string")
      ->setLabel("Mode")
      ->setReadOnly(TRUE)
      ->setDescription(t("The mode of the module during the payment"))
      ->setDefaultValue($config->get("mode")->getValue());

    $default_date = DrupalDateTime::createFromTimestamp(strtotime("now"));
    $default_date = $default_date->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);
    $fields["created"] = BaseFieldDefinition::create("datetime")
      ->setLabel("Created")
      ->setDescription(t("Date that the payment occurred."))
      ->setSettings([
        "datetime_type" => "date"
      ])
      ->setDefaultValue($default_date)
      ->setDisplayOptions("view", [
        "label" => "above",
        "type" => "datetime_default",
        "settings" => [
          "format_type" => "medium",
        ],
        "weight" => 14,
      ])
      ->setDisplayOptions("form", [
        "type" => "datetime",
        "weight" => 14,
      ])
      ->setDefaultValue($default_date)
      ->setDisplayConfigurable("form", TRUE)
       ->setRequired(TRUE)
      ->setDisplayConfigurable("view", TRUE);

    $fields['changed'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Changed'))
      ->setReadOnly(TRUE);

    return $fields;
  }

  public function pay(array $payment_provider_options) {
    $payment_method = $this->loadPaymentMethod();

    $payment = $payment_method->handlePayment($this->invoice_id->getValue()[0]['value'], $this->amount->getValue()[0]['value'], $this->currency->getValue()[0]['value'], $payment_provider_options);

    $this->transaction_key = $payment->getTransactionKey();
    $this->status = $payment->getStatusCode();

    return $payment;
  }

  public function refund(float $amount) {
    $payment_method = $this->loadPaymentMethod();

    $amount_already_refunded = floatval($this->amount_refunded->getValue()[0]['value']);
    if($amount > $this->amount || ($$amount_already_refunded + $amount) > $this->amount)
      throw new Exception(t("Cannot return more than was given!"));

    $this->amount_refunded = $amount_already_refunded + $amount;

    $refund = $payment_method->handleRefund($this->invoice_id->getValue()[0]['value'], $this->transaction_key->getValue()[0]['value'], $amount, $this->currency);

    $this->status = $refund->getStatusCode();

    return $refund;
  }

  protected function loadPaymentMethod() {
    $payment_method_namespace = "Drupal\buckaroo\PaymentMethods\\" . $this->payment_method->getValue()[0]['value'];
    if(!class_exists($payment_method_namespace))
      throw new Exception(t("Payment method does not exist"));

    /** @var \Drupal\buckaroo\PaymentMethod $payment_method */
    $payment_method = new $payment_method_namespace();
    if(!is_a($payment_method, "\Drupal\buckaroo\PaymentMethod"))
      throw new Exception(t("Not a valid payment method"));

    return $payment_method;
  }

  /**
   * {@inheritDoc}
   *
   * Only saves the entity if there has actually been a transaction
   */
  public function save() {
    $transaction_key = $this->getTransactionKey();
    if(!isset($transaction_key) || empty($transaction_key) || in_array($transaction_key, [190, 490, 890, 891, 792, 791, 790, 690, 491]))
      return;

    return parent::save();
  }

  // Get methods
  public function getInvoiceId() {
    if(!isset($this->invoice_id)) return null;
    return $this->invoice_id->getValue()[0]['value'];
  }

  public function getTransactionKey() {
    if(!isset($this->transaction_key)) return null;
    return $this->transaction_key->getValue()[0]['value'];
  }

  public function getAmount() {
    if(!isset($this->amount)) return null;
    return $this->amount->getValue()[0]['value'];
  }

  public function getAmountRefunded() {
    if(!isset($this->amount_refunded)) return null;
    return $this->amount_refunded->getValue()[0]['value'];
  }

  public function getCurrency() {
    if(!isset($this->currency)) return null;
    return $this->currency->getValue()[0]['value'];
  }

  public function getPaymentMethod() {
    if(!isset($this->payment_method)) return null;
    return $this->payment_method->getValue()[0]['value'];
  }

  public function getStatusCode() {
    if(!isset($this->status)) return null;
    return $this->status->getValue()[0]['value'];
  }

  public function getStatus() {
    if(!isset($this->status)) return null;
    $status_code = $this->status->getValue()[0]['value'];
    switch ($status_code) {
      case 190:
        return "success";
      case 490:
        return "failed";
      case 890:
        return "cancelled_by_user";
      case 891:
        return "cancelled_by_merchant";
      case 792:
        return "waiting_on_consumer";
      case 791:
        return "pending_processing";
      case 790:
        return "waiting_on_user_input";
      case 690:
        return "rejected";
      case 491:
        return "validation_failure";
      default:
        return "no_buckaroo_payment_created";
    }
  }

  public function getCreated() {
    if(!isset($this->created)) return null;
    return $this->created->getValue()[0]['value'];
  }

  public function getChanged() {
    if(!isset($this->changed)) return null;
    return $this->changed->getValue()[0]['value'];
  }
}
