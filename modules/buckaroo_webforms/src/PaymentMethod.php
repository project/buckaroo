<?php
namespace Drupal\buckaroo_webforms;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\WebformSubmissionInterface;

interface IPaymentMethod {
  public function adminSettingsFormBuilder(array $config, \Drupal\webform\WebformInterface $webform): array;
  public function getPaymentProviderSpecificProperties(array $config, array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission, \Drupal\webform\WebformInterface $webform): array;
}

abstract class PaymentMethod implements IPaymentMethod {
  /**
   * @see
   */
  protected function t(string $string, array $args = [], array $options = []) {
    return t($string, $args, $options);
  }
}
