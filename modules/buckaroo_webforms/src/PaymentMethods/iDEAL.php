<?php
namespace Drupal\buckaroo_webforms\PaymentMethods;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\buckaroo_webforms\PaymentMethod;

class iDEAL extends PaymentMethod {
 public function adminSettingsFormBuilder(array $config, \Drupal\webform\WebformInterface $webform): array {
    $elements = $webform->getElementsDecodedAndFlattened();
    $options = [];
    foreach ($elements as $key => $element) {
      $options[$key] = $element["#title"];
    }

    $form["issuer_element"] = [
      "#type" => "select",
      "#title" => $this->t("Issuer element"),
      "#description" => $this->t("Form element holding the bank that gets used for the payment."),
      "#required" => false,
      "#options" => ["" => "None"] + $options,
      "#default_value" => $config["method_element"] ?? "",
    ];

    return $form;
 }

 public function getPaymentProviderSpecificProperties(array $config, array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission, \Drupal\webform\WebformInterface $webform): array {
  $options = [
    "returnURL" => \Drupal::request()->getSchemeAndHttpHost() . $webform->getSetting("confirmation_url"),
  ];

  if(isset($config["issuer_element"]) && !empty($config["issuer_element"]))
    $options["issuer"] = $webform_submission->getElementData($config['issuer_element']);

  return $options;
 }
}
