<?php

namespace Drupal\buckaroo_webforms\Plugin\WebformHandler;

use Drupal\buckaroo\Entity\Payment;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 *
 * @WebformHandler(
 *   id = "buckaroo_payment_handler",
 *   label = @Translation("Buckaroo payment"),
 *   category = @Translation("Buckaroo integration"),
 *   description = @Translation("Creates a payment with Buckaroo."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 * )
 */
class BuckarooPaymentHandler extends WebformHandlerBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  protected $availabe_payment_methods;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $class = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $class->entityTypeManager = $container->get('entity_type.manager');
    $class->availabe_payment_methods = $class->getAvailablePaymentMethods();
    return $class;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['settings']['#prefix'] = "<div id='buckaroo_payment_handler_configuration_form'>";
    $form['settings']['#suffix'] = "</div>";

    $form['settings']['general'] = [
      '#type' => 'fieldset',
      '#title' => $this->t("General payment settings")
    ];

    $availabe_payment_methods = [];
    foreach($this->availabe_payment_methods as $val)
      $availabe_payment_methods[$val] = $val;

    $form['settings']['general']['payment_method'] = [
      '#type' => 'select',
      '#title' => $this->t('Payment method'),
      '#required' => TRUE,
      '#options' => $availabe_payment_methods,
      '#default_value' => $this->getConfiguration()['settings']['settings']['general']['payment_method'] ?? '',
    ];

    $form['settings']['general']['currency'] = [
      '#type' => 'select',
      '#title' => $this->t('Currency'),
      '#required' => TRUE,
      '#options' => ['EUR' => 'Euro', 'USD' => 'USD'],
      '#default_value' => $this->getConfiguration()['settings']['settings']['general']['currency'] ?? '',
    ];

    // Build an options array for the elements on the webform.
    $elements = $this->getWebform()->getElementsDecodedAndFlattened();
    $options = [];
    foreach ($elements as $key => $element) {
      $options[$key] = $element['#title'];
    }

    $form['settings']['general']['amount_element'] = [
      '#type' => 'select',
      '#title' => $this->t('Amount element'),
      '#description' => $this->t('Form element holding the amount for the payment.'),
      '#required' => TRUE,
      '#options' => $options,
      '#default_value' => $this->getConfiguration()['settings']['settings']['general']['amount_element'] ?? '',
    ];

    // Get a payment providers specific settings
    $method = $form_state->getValue('payment_method') ?? $this->getConfiguration()['settings']['settings']['general']['payment_method'];
    if(gettype($method) != "string")
      return $form;

    $payment_method = $this->getPaymentMethod($method);
    if(!isset($payment_method))
      return $form;

    $payment_method_form = $payment_method->adminSettingsFormBuilder($this->getConfiguration()['settings']['settings']['payment_method_settings'] ?? [], $this->getWebform());
    if(count($payment_method_form) <= 0)
      return $form;

    $form['settings']['payment_method_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('%payment_method settings', ['%payment_method' => $method]),
    ];

    foreach($payment_method_form as $key => $val)
      $form['settings']['payment_method_settings'][$key] = $val;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $form_values = $form_state->cleanValues()->getValues();

    foreach($form_values as $key => $val)
      $this->configuration[$key] = $val;
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    return [
      '#theme' => 'item_list',
      '#items' => [
        $this->t('Payment method: %payment_method', ['%payment_method' => $this->getConfiguration()['settings']['settings']['general']['payment_method']]),
        $this->t('Currency: %currency', ['%currency' => $this->getConfiguration()['settings']['settings']['general']['currency']]),
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function checkConditions(WebformSubmissionInterface $webform_submission) {
    $conditions_are_met = parent::checkConditions($webform_submission);

    // If not all conditions are met no further checks are need.
    if (!$conditions_are_met) {
      return $conditions_are_met;
    }

    $status = $webform_submission->getElementData("buckaroo_integration_payment_instance_status");

    return !isset($status) && empty($status);
  }

  /**
   * {@inheritdoc}
   */
  public function confirmForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {
    $webform = $this->getWebform();

    $payment = Payment::create([
      "invoice_id" => uniqid(),
      "amount" => (float) $webform_submission->getElementData($this->getConfiguration()['settings']['settings']['general']['amount_element']),
      "currency" => $this->getConfiguration()['settings']['settings']['general']['currency'],
      "payment_method" => $this->getConfiguration()['settings']['settings']['general']['payment_method']
    ]);

    $payment_method = $this->getPaymentMethod($this->getConfiguration()['settings']['settings']['general']['payment_method']);

    $transaction = $payment->pay(
      $payment_method->getPaymentProviderSpecificProperties(
        $this->getConfiguration()['settings']['settings']['payment_method_settings'],
        $form,
        $form_state,
        $webform_submission,
        $webform
      )
    );

    if($transaction == false)
      throw new \Exception("Transaction failed");

    $payment->save();

    $url = $transaction->getRedirectUrl();
    if(!isset($url) || empty($url))
      $url = \Drupal::request()->getSchemeAndHttpHost() . $webform->getSetting("confirmation_url");

    $response = new TrustedRedirectResponse($url, '303');
    $form_state->setResponse($response);
  }

  /**
   * Finds all available payment methods.
   *
   * @return array
   */
  protected function getAvailablePaymentMethods() {
    $glob = glob(dirname(__FILE__) . "/../../PaymentMethods/*.php", GLOB_BRACE);
    $files = [];

    foreach($glob as $file) {
      $class = "Drupal\buckaroo_webforms\PaymentMethods\\" . pathinfo(basename($file), PATHINFO_FILENAME);
      if(class_exists($class) && is_a($class, "\Drupal\buckaroo_webforms\PaymentMethod", true))
        $files[] = pathinfo(basename($file), PATHINFO_FILENAME);
    }

    return $files;
  }

  /**
   * Gets the payment provider class.
   *
   * @param string $payment_method_name - The name of the payment method you want to use.
   * @return null|\Drupal\buckaroo_webforms\PaymentMethod
   */
  protected function getPaymentMethod(string $payment_method_name) {
    if(!in_array($payment_method_name, $this->availabe_payment_methods))
      return null;

    $payment_method_namespace = "Drupal\buckaroo_webforms\PaymentMethods\\" . $payment_method_name;

    if(!class_exists($payment_method_namespace))
      return null;

    /** @var \Drupal\buckaroo_webforms\PaymentMethod $payment_method */
    $payment_method = new $payment_method_namespace();
    if(!is_a($payment_method, "\Drupal\buckaroo_webforms\PaymentMethod", true))
      return null;

    return $payment_method;
  }
}
