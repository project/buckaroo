<?php

namespace Drupal\buckaroo_webforms\Plugin\WebformElement;

use Drupal\buckaroo\Config\Config;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Utility\WebformArrayHelper;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\webform\Plugin\WebformElement\Select;

/**
 * Provides a 'ideal_issuer_selector' element.
 *
 * @WebformElement(
 *   id = "ideal_issuer_selector",
 *   api = "https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Render!Element!Select.php/class/Select",
 *   label = @Translation("Ideal issuer selector"),
 *   description = @Translation("Provides a form element for a drop-down menu from which the user can chooose a issuer."),
 *   category = @Translation("Buckaroo integration"),
 * )
 */
class IdealIssuerSelector extends Select {
  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $config = new Config("buckaroo_integration.settings");

    $buckaroo_client = new \Buckaroo\BuckarooClient(
      $config->get("website_key")->getValue(),
      $config->get("secret_key")->getValue(),
      $config->get("mode")->getValue()
    );

    $issuers = [];
    foreach($buckaroo_client->method('ideal')->issuers() as $value) {
      $issuers[$value["id"]] = $issuers[$value["name"]];
    }

    $form['options']['options'] = [
      '#type' => 'webform_element_options',
      '#title' => $this->t('Options'),
      '#options_description' => $this->hasProperty('options_description_display'),
      '#required' => TRUE,
      '#value' => $issuers,
      '#disabled' => TRUE
    ];

    return $form;
  }

}
